import React, { Component } from 'react';
import ic_cloud from '../../assets/img/upload-to-cloud-48.png';
import ic_pause from '../../assets/img/pause-filled-20.png';
import '../../assets/css/Files.css';

import axios from 'axios';

import { Button, Container, Row, Col, Progress } from 'reactstrap';



class Files extends Component {
    constructor(props) {
      super(props);
      this.textInput = React.createRef();
      this.state = {
        fileOnDropZone: false,
        uploading: [],
        fileList: []
      };
    }

    updateProgress(i, loaded) {
      let state = this.state;
      state.uploading[i].completed = loaded;
      state.uploading[i].completedSize = state.uploading[i].size * (loaded/100);
      this.setState(state);
    }

    startUploadingFiles(files) {
      let state = this.state;
      let currentUploading = state.uploading;
      
      for (let i = 0; i < files.length; i++) {
        let newFile = {
          file: files[i],
          completed: 0,
          completedSize: 0,
          size: files[i].size,
          name: files[i].name,
          status: 0
        };
        currentUploading.push(newFile);
        state.fileList.push(newFile);
      }

      state.uploading = currentUploading;
      
      this.setState(state);
      
      for (let i = 0; i < currentUploading.length; i++) {
        
          let fileIndex = i;
          let c = this;

          const url = 'https://xglv6.mocklab.io/upload';
          const formData = new FormData();
          formData.append('file',currentUploading[i].file);

          const config = {
              onUploadProgress: (progressEvent) => {
                c.updateProgress(fileIndex, Math.round( (progressEvent.loaded * 100) / progressEvent.total ))
              } 
          };
          
          axios.put(url, formData, config).then((response) => {
            // 
            setTimeout(function() {

              let state = c.state;
              state.uploading[fileIndex].status = 1;
              c.setState(state);
            }, 1000);
          });
      }
    }

    selectFilesDialog() {
      this.textInput.current.click();
    }

    filesInputChanged() {
      // start uploading immediately
      let files = this.textInput.current.files;
      this.dropzoneActive(false);
      this.startUploadingFiles(files);
    }

    dropHandler(ev) {
      // Prevent default behavior (Prevent file from being opened)
      ev.preventDefault();
    
      let files = [];
      if (ev.dataTransfer.items) {
        // Use DataTransferItemList interface to access the file(s)
        for (var i = 0; i < ev.dataTransfer.items.length; i++) {
          // If dropped items aren't files, reject them
          if (ev.dataTransfer.items[i].kind === 'file') {
            var file = ev.dataTransfer.items[i].getAsFile();
            files.push(file);
          }
        }
      } else {
        // Use DataTransfer interface to access the file(s)
        for (var j = 0; j < ev.dataTransfer.files.length; j++) {
          files.push(ev.dataTransfer.files[j]);
        }
      } 

      if (files.length > 0) {
        this.startUploadingFiles(files);
      }
      
      // Todo removeDragData for cleanup
    }

    dragOverHandler(ev) {
      // Prevent default behavior (Prevent file from being opened)
      ev.preventDefault();
      this.dropzoneActive(true);
    }

    dropzoneActive(isActive) {
      let s = this.state;
      s.fileOnDropZone = isActive;
      this.setState(s);
    }

    render() {

      let state = this.state;
      let progress = 0;
      let progressSize = 0;
      let numFilesUploading = 0;

      if (state.uploading.length > 0) {
        for (let i = 0; i < state.uploading.length; i++) {
          if (state.uploading[i].status === 0) {
            progress += state.uploading[i].completed;
            progressSize += state.uploading[i].completedSize;
            numFilesUploading++;
          }
        }

        if (progressSize > 0) {
          progressSize = progressSize / 1024 / 1024;
        }
  
        if (numFilesUploading > 0) {
          progress = progress / numFilesUploading;
        }
      }

      var fileListRender = [];
      if (state.fileList.length > 0) {
        for (let j = 0; j < state.fileList.length; j++) {
          let sizeInMb = state.fileList[j].size / 1024 / 1024;
          let fileProgress = state.fileList[j].completed;
          fileListRender.push(
            <Row className="file-list-item">
              <Col md="1">

              </Col>
              <Col md="4">
                <h4>{ state.fileList[j].name }</h4>
                <span>{ sizeInMb.toFixed(2) } MB</span>
              </Col>
              <Col md="5">
                { state.fileList[j].status === 0 ? 
                  <Progress className="upload-bar" animated value={fileProgress}>{fileProgress + "%"}</Progress> :
                  <h4>Uploaded</h4>
                }
              </Col>
              <Col md="1">

              </Col>
              <Col md="1">

              </Col>
            </Row>
          );
        }
      }

      return (
        <Container className="file-upload">
          <input type="file" multiple ref={this.textInput} className="d-none" onChange={this.filesInputChanged.bind(this)} />

          <Row className="py-4">
            <Col>
                <h2>File upload Component</h2>
            </Col>
            <Col>

            </Col>
          </Row>

          <Row>
            <Col>
              <div className={"dropzone " + (this.state.fileOnDropZone ? 'active' : '')} 
                onDrop={this.dropHandler.bind(this)} 
                onDragOver={this.dragOverHandler.bind(this)}
                onDragLeave={this.dropzoneActive.bind(this,false)}>

                <div className="dropzone-text">
                  <img src={ic_cloud} alt="" />
                  Drop files here
                </div>
                <Button className="shadow" color="primary" onClick={this.selectFilesDialog.bind(this)}>or select files</Button>
              </div>
            </Col>
            <Col className="text-right">
              <div className="download-button-wrapper">
                  <Button className="shadow" color="primary">Download all files</Button>
              </div>
            </Col>
          </Row>


          <div className={"mt-4 upload-bulk-info " + (numFilesUploading === 0 ? 'hidden' : '') }>

            <strong className="text-sm">{numFilesUploading} files uploading</strong>

            <div className="upload-bar-wrapper mt-2">
              <div className="controls">
                <img src={ic_pause} alt="" />
              </div>
              <Progress className="upload-bar" animated value={progress}>{progress + "% (" + progressSize.toFixed(2) + "MB)"}</Progress>
            </div>

          </div>


          <div className={"mt-4 mb-4 file-list-table " + (state.fileList.length === 0 ? 'd-none':'') }>
            <Row>
              <Col md="1">
                <strong className="pt-2 d-block">Sort by</strong>
              </Col>
              <Col md="3">
                <select className="form-control">
                  <option>Status</option>
                </select>
              </Col>
            </Row>

            <Row className="header mt-4">
              <Col md="1"></Col>
              <Col md="4">
                Filename
              </Col>
              <Col md="5">
                Status
              </Col>
            </Row>

            {fileListRender}
          </div>

        </Container>
      );
    }
  }
export default Files;
  