import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from './store'

import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem } from 'reactstrap';


import Home from "./components/pages/Home";
import Files from "./components/pages/Files";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
      
          <div className="App">
            <Navbar color="light" light expand="md">
              <NavbarBrand href="/">Scripto</NavbarBrand>
              <Nav className="ml-auto" navbar>
                  <NavItem>
                    <Link to="/">Home</Link>
                  </NavItem>
                  <NavItem>
                    <Link to="/files">Files</Link>
                  </NavItem>
                </Nav>
            </Navbar>

            <Route exact path="/" component={Home} />
            <Route exact path="/files" component={Files} />
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
